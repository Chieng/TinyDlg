#include "TinyDlg.h"

LRESULT CALLBACK proc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam){
	switch (msg)
	{
	case WM_LBUTTONDOWN:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	POINT wndPos = { 100, 100 };
	TinyDlg td(hInstance, "test", wndPos, "..\\img\\test.bmp");

	POINT btnPos1 = { 0, 0 };
	TinyBtn tb1(hInstance, "btnA", btnPos1, "..\\img\\btnA.bmp");

	POINT btnPos2 = { 240, 300 };
	TinyBtn tb2(hInstance, "btnB", btnPos2, "..\\img\\btnB.bmp");

	POINT btnPos3 = {240, 0};
	TinyBtn tb3(hInstance, "btnC", btnPos3, "..\\img\\btnC.bmp");
	tb3.SetWndProc(proc);
	
	td.AppendCtrl(&tb1);
	td.AppendCtrl(&tb2);
	td.AppendCtrl(&tb3);

	td.Create(NULL);	

	TinyDlg::MsgLoop();

	return 0;
}