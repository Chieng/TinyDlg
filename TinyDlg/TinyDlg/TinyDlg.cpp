#include "TinyDlg.h"

/*********************************************************************************************
*	TinyBase 成员函数定义
**********************************************************************************************/
TinyBase::TinyBase(HINSTANCE hInstance, POINT pos, const char* skinPath)
{
	m_hInstance = hInstance;

	m_skin = (HBITMAP)LoadImage(0, skinPath, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	BITMAP bmp;
	GetObject(m_skin, sizeof(BITMAP), &bmp);

	m_pos = pos;
	m_size = { bmp.bmWidth, bmp.bmHeight };
	m_brush = CreatePatternBrush(m_skin);
}

TinyBase::~TinyBase()
{
	DeleteObject(m_skin);
	DeleteObject(m_brush);
}

void TinyBase::SetWndProc(WNDPROC proc)
{ 	
	if (proc == NULL)
	{
		defaultProc();
	}
	else if (m_hWnd == NULL)
	{
		m_wcex.lpfnWndProc = proc;
		m_wndProc = proc;
	}
	else
	{
		SetWindowLong(m_hWnd, GWL_WNDPROC, (LONG)proc);
		m_wndProc = proc;
		m_wcex.lpfnWndProc = proc;
	}	
}

/*********************************************************************************************
*	TinyDlg 成员函数定义
**********************************************************************************************/

// 初始化m_wcex, m_skin, m_pos, m_size;
TinyDlg::TinyDlg(HINSTANCE hInstance, const char * wndName, POINT pos, const char* skinPath)
: TinyBase(hInstance, pos, skinPath)
{	
	SetWndProc(NULL);
	initWNDCLASSEx(hInstance, wndName);
}

// 销毁窗口
TinyDlg::~TinyDlg()
{
	for (int i = m_ctrls.size() - 1; i >= 0; i--)
		DestroyWindow(m_ctrls[i]->GetHWND());		

	if (m_hWnd != NULL && IsWindow(m_hWnd))
	{		
		DestroyWindow(m_hWnd);
	}
}

// 设置默认的消息处理函数
void TinyDlg::defaultProc()
{
	m_wndProc = [](HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_LBUTTONDOWN:
			SendMessage(hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		}
		return DefWindowProc(hWnd, msg, wParam, lParam);
	};
}

// 实现TinyBase的接口，初始化窗口控制类
void TinyDlg::initWNDCLASSEx(HINSTANCE hInstance, const char* wndName)
{
	m_wcex.cbSize = sizeof(WNDCLASSEX);
	m_wcex.style = CS_HREDRAW | CS_VREDRAW;
	m_wcex.lpfnWndProc = m_wndProc;
	m_wcex.cbClsExtra = 0;
	m_wcex.cbWndExtra = 0;
	m_wcex.hInstance = hInstance;
	m_wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	m_wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	m_wcex.hbrBackground = m_brush;
	m_wcex.lpszMenuName = NULL;
	m_wcex.lpszClassName = wndName;
	m_wcex.hIconSm = LoadIcon(m_wcex.hInstance, IDI_APPLICATION);
}

// 创建主窗口，根据m_ctrls创建控件
HWND TinyDlg::Create(HWND wndParent)
{
	if (RegisterClassEx(&m_wcex) == 0)
	{
		MessageBox(m_hWnd, "Register WNDCLASSEX fail", "error", 0);
		exit(GetLastError());
	}

	m_hWnd = CreateWindowEx(WS_EX_LAYERED, m_wcex.lpszClassName,
		m_wcex.lpszClassName, WS_POPUP,
		m_pos.x, m_pos.y, m_size.cx, m_size.cy,
		wndParent, NULL, m_hInstance, NULL);

	SetLayeredWindowAttributes(m_hWnd, TransparentColor, 0, LWA_COLORKEY);

	if (m_hWnd == NULL)
	{
		MessageBox(m_hWnd, "Create Dialog fail", "error", 0);
		exit(GetLastError());
	}

	for (int i = m_ctrls.size() - 1; i >= 0; i--)
		m_ctrls[i]->Create(m_hWnd);

	ShowWindow(m_hWnd, SW_SHOWNORMAL);
	UpdateWindow(m_hWnd);

	return m_hWnd;
}

// 添加控件，方便创建控件
void TinyDlg::AppendCtrl(TinyCtrl* ctrl)
{
	m_ctrls.push_back(ctrl);
}

// 静态函数，获取消息
int TinyDlg::MsgLoop()
{
	MSG msg;
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			WaitMessage();
		}
	}
	return msg.wParam;
}


/*********************************************************************************************
*	TinyBtn 成员函数定义
**********************************************************************************************/
TinyCtrl::TinyCtrl(HINSTANCE hInstance, POINT pos, const char* skinPath)
: TinyBase(hInstance, pos, skinPath)
{}


/*********************************************************************************************
*	TinyBtn 成员函数定义
**********************************************************************************************/

// 初始化m_wcex, w_skin, w_rect;
TinyBtn::TinyBtn(HINSTANCE hInstance, const char * wndName, POINT pos, const char* skinPath)
: TinyCtrl(hInstance, pos, skinPath)
{	
	defaultProc();	
	initWNDCLASSEx(hInstance, wndName);	
	InitRegion();
}

TinyBtn::~TinyBtn()
{
	DeleteObject(m_region);
	if (m_hWnd != NULL && IsWindow(m_hWnd))
	{
		DestroyWindow(m_hWnd);
	}
}

void TinyBtn::InitRegion()
{		
	m_region = NULL;
	BOOL first = TRUE;
	HDC hdc = CreateCompatibleDC(NULL);
	SelectObject(hdc, m_skin);
	for (int y = 0; y < m_size.cy; y++)
	{
		for (int x = 0; x < m_size.cx; x++) {
			if (GetPixel(hdc, x, y) == TransparentColor)
			{
				// 嵌套太多会晕
			}
			else if (first == TRUE)
			{
				first = FALSE;
				m_region = CreateRectRgn(x, y, x + 1, y + 1);
			}
			else
			{
				HRGN temp = CreateRectRgn(x, y, x + 1, y + 1);				
				CombineRgn(m_region, m_region, temp, RGN_OR);
				DeleteObject(temp);
			}		
		}
	}
	DeleteDC(hdc);
}

void TinyBtn::defaultProc()
{
	m_wndProc = [](HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_LBUTTONDOWN:
		{
							   char buf[MAXBYTE] = { 0 };
							   GetClassName(hWnd, buf, _countof(buf));
							   MessageBox(hWnd, buf, "Button's Name", 0);
		}
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		}
		return DefWindowProc(hWnd, msg, wParam, lParam);
	};
}

void TinyBtn::initWNDCLASSEx(HINSTANCE hInstance, const char* wndName)
{
	m_wcex.cbSize = sizeof(WNDCLASSEX);
	m_wcex.style = CS_HREDRAW | CS_VREDRAW;
	m_wcex.lpfnWndProc = m_wndProc;
	m_wcex.cbClsExtra = 0;
	m_wcex.cbWndExtra = 0;
	m_wcex.hInstance = hInstance;
	m_wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	m_wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	m_wcex.hbrBackground = m_brush;
	m_wcex.lpszMenuName = NULL;
	m_wcex.lpszClassName = wndName;
	m_wcex.hIconSm = LoadIcon(m_wcex.hInstance, IDI_APPLICATION);
}

HWND TinyBtn::Create(HWND wndParent)
{
	if (RegisterClassEx(&m_wcex) == 0)
	{
		MessageBox(m_hWnd, "Register WNDCLASSEX fail", "error", 0);
		exit(GetLastError());
	}

	m_hWnd = CreateWindowEx(0, m_wcex.lpszClassName,
		m_wcex.lpszClassName, WS_CHILD,
		m_pos.x, m_pos.y, m_size.cx, m_size.cy,
		wndParent, NULL, m_hInstance, NULL);

	if (m_hWnd == NULL)
	{
		MessageBox(m_hWnd, "Create btn Dialog fail", "error", 0);
		exit(GetLastError());
	}

	SetWindowRgn(m_hWnd, m_region, TRUE);

	ShowWindow(m_hWnd, SW_SHOWNORMAL);	

	return m_hWnd;
}
