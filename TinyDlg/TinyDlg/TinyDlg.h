/*
* 类间关系说明： --> 表示继承, ==> 表示包含（组合）关系;
* TinyDlg --> TinyBase;
* TinyBtn --> TinyCtrl --> TinyBase;
* TinyDlg ==> TinyCtrl;
*/

#pragma once

#include <Windows.h>
#include <vector>

class TinyBase
{
public:
	TinyBase(HINSTANCE hInstance, POINT pos, const char* skinPath);
	virtual ~TinyBase();

	virtual HWND Create(HWND wndParent) = 0;	

	HWND GetHWND() { return m_hWnd; }

	void SetWndProc(WNDPROC proc);

protected:
	HBITMAP m_skin;
	POINT m_pos;
	SIZE m_size;
	WNDPROC m_wndProc;
	HWND m_hWnd;
	HINSTANCE m_hInstance;
	HBRUSH m_brush;
	WNDCLASSEX m_wcex;
	
	virtual void defaultProc() = 0;	
	virtual void initWNDCLASSEx(HINSTANCE hInstance, const char* wndName) = 0;

	static const DWORD TransparentColor = RGB(255, 255, 255);
};
//==========================================================================================================================

class TinyCtrl;
class TinyDlg : public TinyBase
{
public:
	TinyDlg(HINSTANCE hInstance, const char * wndName, POINT pos, const char* skinPath);	// 初始化m_wcex, m_skin, m_pos, m_size;
	virtual ~TinyDlg();

	HWND Create(HWND wndParent);	// 创建主窗口，根据m_ctrls创建控件		

	void AppendCtrl(TinyCtrl* ctrl);
	
	static int MsgLoop();

protected:
	void defaultProc();
	void initWNDCLASSEx(HINSTANCE hInstance, const char* wndName);

private:		
	std::vector<TinyCtrl*> m_ctrls;
};
//===============================================================================================================

class TinyCtrl : public TinyBase
{
protected:	
	TinyCtrl(HINSTANCE hInstance, POINT pos, const char* skinPath);	
	virtual void InitRegion() = 0;	

	HRGN m_region;
};
//===============================================================================================================

class TinyBtn :public TinyCtrl
{
public:
	TinyBtn(HINSTANCE hInstance, const char * wndName, POINT pos, const char* skinPath);		// 初始化m_wcex, w_skin, w_rect;
	virtual ~TinyBtn();

	HWND Create(HWND wndParent);	
	
protected:	
	void defaultProc();
	void InitRegion();
	void initWNDCLASSEx(HINSTANCE hInstance, const char* wndName);		
};
